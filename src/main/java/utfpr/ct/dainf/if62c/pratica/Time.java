/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author leandro
 */
public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    
    public Time() {
        
    }
    
    public HashMap<String, Jogador> getJogadores() {
        return this.jogadores;
    }
    
    /**
     *
     * @param posicao Posição que o jogador atua
     * @param jogador Objeto da classe Jogador
     */
    public void addJogador(String posicao, Jogador jogador) {
        jogadores.put(posicao, jogador);
    }

}
