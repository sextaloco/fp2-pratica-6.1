
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        Jogador fulano = new Jogador(1, "Fulano");
        Jogador joao = new Jogador(1, "João");
        Jogador ciclano = new Jogador(4, "Ciclano");
        Jogador jose = new Jogador(7, "José");
        Jogador beltrano = new Jogador(10, "Beltrano");
        Jogador mario = new Jogador(15, "Mario");
        
        time1.addJogador("Goleiro", fulano);
        time1.addJogador("Lateral", ciclano);
        time1.addJogador("Atacante", beltrano);
        
        time2.addJogador("Goleiro", joao);
        time2.addJogador("Lateral", jose);
        time2.addJogador("Atacante", mario);
        
        System.out.println("Posição\tTime1\tTime2");
        Set<Map.Entry<String, Jogador>> jogadoresTime1 = time1.getJogadores().entrySet();
        Set<Map.Entry<String, Jogador>> jogadoresTime2 = time2.getJogadores().entrySet();
        for (Map.Entry<String, Jogador> jogadorTime1: jogadoresTime1) {
            for (Map.Entry<String, Jogador> jogadorTime2: jogadoresTime2) {
                if (jogadorTime1.getKey().equals(jogadorTime2.getKey())) {
                    System.out.println(jogadorTime1.getKey() + "\t" + jogadorTime1.getValue() + "\t" + jogadorTime2.getValue());
                }
            }
           
        }   
    }
}
